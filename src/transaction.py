import copy as cp
import pandas as pd
from functools import reduce
from typing import List, Union, Callable, Dict

# This module converts the input data from the POST query to a format acceptable by the model
# Index(['ID_USER', 'genero', 'monto', 'fecha', 'hora', 'dispositivo',
#        'establecimiento', 'ciudad', 'tipo_tc', 'linea_tc', 'interes_tc',
#        'status_txn', 'is_prime', 'dcto', 'cashback', 'fraude'],
#       dtype='object')

_features = {
    'genero': str, 
    'monto': float, 
    'fecha': str, 
    'hora': int, 
    'dispositivo': dict,
    'establecimiento': str, 
    'ciudad': str, 
    'tipo_tc': str, 
    'linea_tc': int, 
    'interes_tc': int,
    'status_txn': str, 
    'is_prime': bool, 
    'dcto': float, 
    'cashback': float
}

def check_transaction_data (data: dict) -> bool:
    has_all_keys = all(map(lambda p: p[0] == p[1], zip(data, _features)))
    has_correct_types = all(map(
        lambda entry: type(entry[1]) is _features[entry[0]],
        data.items()
    ))
    return has_all_keys and has_correct_types

def make_transaction (data: dict) -> Union[dict, str]:
    if check_transaction_data(data):
        [year, month, day] = list(map(int, data["fecha"].split("-")))
        device = data["dispositivo"]
        transaction = cp.deepcopy(data)
        transaction.update({
            "year": year, "month": month, "day": day,
            "device_score": device["device_score"], "device_os": device["os"]
        })
        del transaction["fecha"]
        del transaction["dispositivo"]
        return transaction
    else:
        return "The given data does not have the correct data types or has missing fields"

def make_transactions (data: List[dict]) -> Union[List[dict], str]:
    transactions= list(map(make_transaction, data))
    first_str = next(
        filter(lambda t: type(t) is str, transactions),
        False
    )
    return first_str if first_str else transactions

def encode_column (X: pd.DataFrame) -> Callable[[str], Dict[str, int]]:
    def encode (col: str) -> Dict[str, int]:
        unq = X[col].unique()
        return dict(zip(unq, range(0, unq.shape[0])))
    return encode

def transactions_to_dataframe (data: List[dict]) -> Union[List[dict], str]:
    def merge_entries (acc: dict, d: dict):
        for k, v in d.items():
            if k in acc:
                acc[k].append(v)
            else:
                acc[k] = [v]
        return acc
    transactions = make_transactions(data)
    if type(transactions) is str:
        return transactions
    else:
        merged_dict = reduce(merge_entries, transactions, dict())
        fraud_df = pd.DataFrame.from_dict(merged_dict)
        fraud_obj_cols = fraud_df.select_dtypes(include=["object"]).columns
        encoder = encode_column(fraud_df)
        encoded = dict(zip(
            fraud_obj_cols,
            map(encoder, fraud_obj_cols)
        ))
        fraud_df = fraud_df.replace(encoded)
        return fraud_df

