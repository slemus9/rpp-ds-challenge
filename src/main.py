import os
import flask
import pickle
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from transaction import transactions_to_dataframe

root_path = os.path.abspath("")

# Model
model_path = f"{root_path}/data/randomForestModel"

def load_model (path: str) -> RandomForestClassifier:
    with open(path, "rb") as f:
        return pickle.load(f)

_model = load_model(model_path)

# Server
app = flask.Flask(root_path)
app.config["DEBUG"] = True

# Home route
@app.route("/", methods=["GET"])
def root ():
    return "<h1>Fraud Classifier</h1>"

# Error 404
@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

# Predict a list of queries
@app.route("/fraud/predict", methods=["POST"])
def predict_fraud ():
    data = flask.request.json["data"]
    transactions = transactions_to_dataframe(data)
    pred = _model.predict(transactions)
    transactions["fraude"] = pred
    transactions_jsn = pd.DataFrame.to_json(transactions, orient="records")
    print(transactions)
    return transactions_jsn

# Run Server
app.run()