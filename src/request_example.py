import os
import json
import ast
import requests
import pandas as pd

root_path = os.path.abspath("")
fraud_path = f"{root_path}/data/ds_challenge_2021.csv"
fraud_df = pd.read_csv(fraud_path)
fraud_df = fraud_df.fillna("UNK")
# Drop user ID
fraud_df = fraud_df.drop("ID_USER", axis=1)

# Get 6 examples, 3 of each category
legit_examples = fraud_df[~fraud_df.fraude].head(n=3)
fraud_examples = fraud_df[fraud_df.fraude].head(n=3)
examples = pd.concat([legit_examples, fraud_examples])
print("Examples")
print(examples)
# Pop fraud value
examples = examples.drop("fraude", axis=1)
# Parse from Dataframe to Dictionary
examples_jsn = json.loads(pd.DataFrame.to_json(examples, orient="records"))
for d in examples_jsn:
    d["dispositivo"] = ast.literal_eval(d["dispositivo"])

# Do a request to the server. Run first the src/main.py file
host = "localhost"
port = 5000
body = {"data": examples_jsn}
req = requests.post(
    f"http://{host}:{port}/fraud/predict",
    json=body
)

print("Result of Request")
print(req.content)