#from distutils.core import setup
from setuptools import setup, find_packages

#print(find_packages())

setup(
    name='rpp-ds-challenge',
    version='0.1dev',
    packages=find_packages(),
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
)