# Rappi Challenge

## Setup
All necessary libraries to run this package are located in `./requirements.txt`. This project was developed in Python 3.8.
It's recommended to create a virtual environment before installing the dependencies. A way to create and activate a virtual
environment is the following:

```
$ cd /path/to/rpp-ds-challenge
$ virtualenv venv
$ source venv/bin/activate
```
To install the dependencies run:

```
$ pip install -r requirements.txt
```

Check that you're running in the virtual environment with

```
$ which python
$ which pip
```

## Run
To start the server with the REST API to query for predictions run:

```
$ python src/main.py
```
This will run a server in your localhost. To test it you need to send a **POST** request to:

```
http://localhost:5000/fraud/predict
```

With a json body with the following structure:

```
{
    "data": [
        {'genero': 'F', 'monto': 608.3456335343, 'fecha': '2020-01-21', 'hora': 20, 'dispositivo': {'model': 2020, 'device_score': 3, 'os': 'ANDROID'}, 'establecimiento': 'Super', 'ciudad': 'Merida', 'tipo_tc': 'Física', 'linea_tc': 71000, 'interes_tc': 51, 'status_txn': 'Aceptada', 'is_prime': False, 'dcto': 60.8345633534, 'cashback': 5.4751107018}, {'genero': 'F', 'monto': 88.7192428942, 'fecha': '2020-01-15', 'hora': 7, 'dispositivo': {'model': 2020, 'device_score': 1, 'os': 'ANDROID'}, 'establecimiento': 'UNK', 'ciudad': 'Merida', 'tipo_tc': 'Virtual', 'linea_tc': 71000, 'interes_tc': 51, 'status_txn': 'Aceptada', 'is_prime': False, 'dcto': 0.0, 'cashback': 1.7743848579},
        {...},
        ...
    ]
}

```
you can send as many queries as you like inside the "data" array. You can try this on postman or, to avoid copying all the fields in a json, you can follow
and execute the example at `./src/request_example.py` (you have to run `./src/main.py` beforehand)

```
$ python src/request_example.py
```
Inside the file `./src/request_example.py` it is showed how you could transform a `.csv` with the data to the required json, and then send a request to the server.

**NOTE:** If you have trouble with the imports in the modules and/or notebooks, you may need to run the following command:

```
$ pip install .
```

## Notebooks
There are 3 notebooks that explaing the reasoning behing the constructed model:

- `./notebooks/data_exploration.ipynb`: Shows the diferent features and their relation to the target.
- `./notebooks/client_categorization.ipynb`: Shows a way in which we can group clients by certain categories.
- `./notebooks/fraud_classification.ipynb`: Shows a way to construct a model for predicting fraud based on the data exploration.

